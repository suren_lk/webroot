package com.home.surenr.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.home.surenr.dao.User;

@Controller
public class HomeController {

	@RequestMapping("home")
	public ModelAndView homeObj(User user) {
		ModelAndView mv = new ModelAndView();
		mv.addObject("obj" , user);
		mv.setViewName("home");
		return mv;
	}
	
	@RequestMapping("homeSession")
	public ModelAndView home(HttpSession session, @RequestParam("name") String name) {
		ModelAndView mv = new ModelAndView();
		mv.addObject("name" , name);
		mv.setViewName("home");
		return mv;
	}
	
	@RequestMapping("homeString")
	public String homeString(HttpSession session, @RequestParam("name") String name) {
		System.out.println(name);
		System.out.println("hi");
		session.setAttribute("name", name);
		return "home";
	}

}
